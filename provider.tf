terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "s3" {
    bucket = "terraform-devops.hycloud.tfstate"
    key    = "batchb.tfstate"
    region = "us-east-1"
    dynamodb_table = "hycloudlocking"
    
  }

}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
}

